<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoCasa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_casas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('tipologia_casa');
        });

        Schema::create('tipo_casa_projecto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('quantidade');
            $table->unsignedBigInteger('tipo_casa_id');
            $table->foreign('tipo_casa_id')->references('id')->on('tipo_casas');
            $table->unsignedBigInteger('projecto_id');
            $table->foreign('projecto_id')->references('id')->on('projectos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_casa_projecto');
        Schema::dropIfExists('tipo_casas');
    }
}
