<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTipoCasaProjectoToCasaProjecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
private $from = 'tipo_casa_projecto';
private $to = 'casa_projectos';

    public function up()
    {
        Schema::rename($this->from, $this->to);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename($this->to, $this->from);
    }
}
