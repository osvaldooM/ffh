<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCasaProjectoToEntrega extends Migration
{

    private $from = 'casa_projectos';
    private $to = 'entregas';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename($this->from, $this->to);
        Schema::table($this->to, function (Blueprint $table) {
            $table->string('ano');
            $table->string('tipo_produto');
            $table->string('modelo_habitacao');
            $table->text('notas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->to, function (Blueprint $table) {
            $table->dropColumn(['tipo_produto', 'modelo_habitacao', 'notas']);
        });
        Schema::rename($this->to, $this->from);
    }
}
