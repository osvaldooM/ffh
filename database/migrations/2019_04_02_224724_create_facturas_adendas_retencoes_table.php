<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturasAdendasRetencoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('designacao');
            $table->float('valor', 15);
            $table->dateTime('data_pagamento');
            $table->unsignedBigInteger('projecto_id');
            $table->foreign('projecto_id')->references('id')->on('projectos');
        });
        Schema::create('adendas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('designacao');
            $table->float('valor', 15);
            $table->dateTime('data_adenda');
            $table->unsignedBigInteger('projecto_id');
            $table->foreign('projecto_id')->references('id')->on('projectos');
        });
        Schema::create('retencoes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('designacao');
            $table->float('valor', 15);
            $table->dateTime('data_retencoes');
            $table->unsignedBigInteger('projecto_id');
            $table->foreign('projecto_id')->references('id')->on('projectos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
        Schema::dropIfExists('adendas');
        Schema::dropIfExists('retencoes');
    }
}
