<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projectos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('designacao');
            $table->string('modelo_de_habitacao');
            $table->string('publico_alvo');
            $table->string('local');
            $table->string('coordenadas');
            $table->string('informacao_do_terreno');
            $table->string('empreiteiro');
            $table->string('financiador');
            $table->string('designacao_financiador');
            $table->string('fiscal');
            $table->string('descriccao');
            $table->dateTime('data_inicio');
            $table->dateTime('data_conclusao');
            $table->longText('observacao');
            $table->string('avaliacao_projecto');
            $table->string('estado');
            $table->float('valor_contrato', 16);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projectos');
    }
}
