<?php

use Illuminate\Database\Seeder;

class ProjectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projectos')->insert([
            [
                'created_at' => '2018-09-12 00:00:00',
                'updated_at' => '2018-09-12 00:00:00',
                'designacao' => 'Habita Moz - Zintava II',
                'modelo_de_habitacao' => 'Apartamento',
                'publico_alvo' => 'Média Alta Renda',
                'local' => 'Zintava',
                'coordenadas' => '90:29:44:14',
                'informacao_do_terreno' => 'Zintava Distrito de Marracuene 900/700',
                'empreiteiro' => 'Soares da Costa',
                'financiador' => 'Parceria',
                'designacao_financiador' => 'CCM',
                'fiscal' => 'Equipe Interna',
                'descriccao' => 'Projeco etc etc',
                'data_inicio' => '2018-09-12 00:00:00',
                'data_conclusao' => '2019-04-17 00:00:00',
                'observacao' => 'observacao',
                'avaliacao_projecto' => 'Bom',
                'estado' => 'Em curso',
                'valor_contrato' => '8000000',
                'districto_id' => '44'


            ]
        ]);
    }
}
