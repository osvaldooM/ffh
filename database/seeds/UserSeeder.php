<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => env('APP_DEFAULT_USER_NAME'),
            'email' => env('APP_DEFAULT_USER_EMAIL'),
            'password' => bcrypt(env('APP_DEFAULT_USER_PASSWORD')),
        ]);
    }
}
