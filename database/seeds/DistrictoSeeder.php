<?php

use Illuminate\Database\Seeder;

class DistrictoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Eloquent::unguard();

        $path = 'database/data/districtos.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Districts table seeded!');
    }
}
