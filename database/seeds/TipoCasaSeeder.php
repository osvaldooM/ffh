<?php

use Illuminate\Database\Seeder;

class TipoCasaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_casas')->insert([
            [
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'tipologia_casa' => 'tipo 0'
            ],
            [
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'tipologia_casa' => 'tipo 1'
            ],
            [
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'tipologia_casa' => 'tipo 2'
            ],
            [
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'tipologia_casa' => 'tipo 3'
            ],
            [
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'tipologia_casa' => 'tipo 4'
            ],
            [
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'tipologia_casa' => 'geminada'
            ],
            [
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'tipologia_casa' => 'Não aplicavel'
            ]
        ]);
    }
}
