<?php

use Illuminate\Database\Seeder;

class ProvinciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Eloquent::unguard();

        $path = 'database/data/provincias.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Provinces table seeded!');
    }
}
