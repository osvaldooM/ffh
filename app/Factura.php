<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    public function projecto()
    {
        return $this->belongsTo('App\Projecto');
    }
}
