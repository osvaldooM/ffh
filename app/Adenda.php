<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adenda extends Model
{
    public function projecto()
    {
        return $this->belongsTo('App\Projecto');
    }
}
