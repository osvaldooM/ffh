<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCasa extends Model
{
    //
    public function projectos() {
        return $this->belongsToMany('App\Projecto', 'casa_projectos','projecto_id','tipo_casa_id');
    }
}
