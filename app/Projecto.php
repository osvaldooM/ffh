<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projecto extends Model
{
    protected $fillable = [
        'designacao',
        'modelo_de_habitacao',
        'publico_alvo',
        'local',
        'districto',
        'coordenadas',
        'informacao_do_terreno',
        'empreiteiro',
        'financiador',
        'fiscal',
        'descriccao',
        'data_inicio',
        'data_conclusao',
        'observacao',
        'avaliacao_projecto',
        'estado',
        'valor_contrato',
        'designacao_financiador'
    ];
    //

    public function facturas()
    {
        return $this->hasMany(Factura::class);
    }
    public function entregas()
    {
        return $this->hasMany(Entrega::class);
    }

    public function tipo_casas()
    {
        return $this->hasManyThrough(TipoCasa::class, Entrega::class);
    }
    public function casas() {
        return $this->belongsToMany('App\TipoCasa', 'casa_projectos','projecto_id','tipo_casa_id')
            ->withPivot([
                'quantidade'
            ]);
    }
}
