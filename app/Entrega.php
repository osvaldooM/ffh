<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrega extends Model
{
    //
    public function tipo_casa()
    {
        return $this->belongsTo(TipoCasa::class,'tipo_casa_id');
    }

}
