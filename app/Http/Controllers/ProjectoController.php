<?php

namespace App\Http\Controllers;

use App\Entrega;
use App\Projecto;
use App\Provincia;
use App\TipoCasa;
use Illuminate\Http\Request;

class ProjectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Projecto::all();
        return view('pages/projectos/index', ['projects' => $projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provincias = Provincia::all();
        return view('pages/projectos/create', ['provincias' => $provincias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $project =  Projecto::create($request->all());
       //return json_encode($project);
        return redirect()->route('details_projectos', ['id' => $project->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Projecto  $projecto
     * @return \Illuminate\Http\Response
     */
    public function show($projectoId)
    {
        $projecto = Projecto::with('entregas', 'entregas.tipo_casa')->findOrFail($projectoId);
//        dd($projecto->entregas()->get()[0]->tipo_casas()->get());
        $tipo_casas = TipoCasa::all();
        return view('pages/projectos/details', ['projecto' => $projecto, 'tipo_casas' => $tipo_casas]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Projecto  $projecto
     * @return \Illuminate\Http\Response
     */
    public function edit(Projecto $projecto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Projecto  $projecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Projecto $projecto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Projecto  $projecto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Projecto $projecto)
    {
        //
    }

    public function add_entregas(Request $request) {
        $entrega = new Entrega();
        $entrega->projecto_id = $request->projecto_id;
        $entrega->ano = $request->ano;
        $entrega->tipo_produto = $request->tipo_produto;
        $entrega->modelo_habitacao = $request->modelo_habitacao;
        $entrega->tipo_casa_id = $request->tipo_casa_id;
        $entrega->quantidade = $request->quantidade;

        if($entrega->save()) {
            return redirect()->back();
        } else {
            return redirect()->back(['entrega' => $entrega]);
        }
    }
}
