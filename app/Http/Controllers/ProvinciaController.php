<?php

namespace App\Http\Controllers;

use App\Districto;
use Illuminate\Http\Request;

class ProvinciaController extends Controller
{
    //
    public function districtos($provincia_id) {
//        dd($provincia_id);
        return json_encode(Districto::where('provincia_id', $provincia_id)->get());
    }
}
