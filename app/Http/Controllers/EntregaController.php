<?php

namespace App\Http\Controllers;

use App\Entrega;
use Illuminate\Http\Request;

class EntregaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entrega  $casaProjecto
     * @return \Illuminate\Http\Response
     */
    public function show(Entrega $casaProjecto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entrega  $casaProjecto
     * @return \Illuminate\Http\Response
     */
    public function edit(Entrega $casaProjecto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entrega  $casaProjecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entrega $casaProjecto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entrega  $casaProjecto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entrega $casaProjecto)
    {
        //
    }
}
