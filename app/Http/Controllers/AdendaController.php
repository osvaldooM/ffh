<?php

namespace App\Http\Controllers;

use App\Adenda;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($projecto_id)
    {
        $adendas_db = Adenda::where('projecto_id',$projecto_id);
        $adendas = $adendas_db->get();
        $valor_sum = $adendas_db->sum('valor');

        return view("pages/adendas/index", ['adendas' => $adendas,"projecto_id" => $projecto_id, 'valor_sum' => $valor_sum]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adenda = new Adenda();
        $adenda->data_adenda = $request->data_pagamento;
        $adenda->projecto_id = $request->projecto_id;
        $adenda->valor = $request->valor;
        $adenda->designacao = $request->designacao;
        if(!empty($request->anexo)) {
            $adenda->anexo = $request->file('anexo')->store('anexos/facturas');
        }


        if($adenda->save()) {
            return redirect()->route('view_adendas', ['projectId' => $adenda->projecto_id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Adenda  $adenda
     * @return \Illuminate\Http\Response
     */
    public function show(Adenda $adenda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Adenda  $adenda
     * @return \Illuminate\Http\Response
     */
    public function edit(Adenda $adenda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Adenda  $adenda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Adenda $adenda)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Adenda  $adenda
     * @return \Illuminate\Http\Response
     */
    public function destroy(Adenda $adenda)
    {
        //
    }

    //TODO store filename instead of path.(path is likely to change)
    public function get_anexos($adenda_id) {
        $adenda = Adenda::with('projecto')->findOrFail($adenda_id);
        $filename = 'adenda-' . $adenda->projecto->designacao;
        $file_extension = preg_replace("#\?.*#", "", pathinfo($adenda->anexo, PATHINFO_EXTENSION));

        return Storage::download($adenda->anexo, $filename . ".$file_extension");
    }
}
