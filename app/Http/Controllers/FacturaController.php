<?php

namespace App\Http\Controllers;

use App\Factura;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($projecto_id)
    {
        $facturas_db = Factura::where('projecto_id',$projecto_id);
        $facturas = $facturas_db->get();
        $valor_sum = $facturas_db->sum('valor');

        return view("pages/facturas/index", ['facturas' => $facturas,"projecto_id" => $projecto_id, 'valor_sum' => $valor_sum]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $factura = new Factura();
        $factura->data_pagamento = $request->data_pagamento;
        $factura->projecto_id = $request->projecto_id;
        $factura->valor = $request->valor;
        $factura->designacao = $request->designacao;
        if(!empty($request->anexo)) {
            $factura->anexo = $request->file('anexo')->store('anexos/facturas');
        }

        if($factura->save()) {
            return redirect()->route('view_facturas', ['projectId' => $factura->projecto_id]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function show(Factura $factura)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function edit(Factura $factura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Factura $factura)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function destroy(Factura $factura)
    {
        //
    }

    //TODO store filename instead of path.(path is likely to change)
    public function get_anexos($factura_id) {
        $factura = Factura::with('projecto')->findOrFail($factura_id);
        $filename = 'factura-' . $factura->projecto->designacao;
        $file_extension = preg_replace("#\?.*#", "", pathinfo($factura->anexo, PATHINFO_EXTENSION));

        return Storage::download($factura->anexo, $filename . ".$file_extension");
    }
}
