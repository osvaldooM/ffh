<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});




Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/projectos','ProjectoController@index');
    Route::get('/','ProjectoController@index');

    Route::get('/projectos/create','ProjectoController@create')->name('create_project');

    Route::post('/projectos/create', 'ProjectoController@store');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/projectos/{projectId}', 'ProjectoController@show')->name('details_projectos');

    Route::get('/projectos/{projectId}/facturas', 'FacturaController@index')->name('view_facturas');
    Route::get('/facturas/{factura_id}/anexos', 'FacturaController@get_anexos')->name('anexos_facturas');
    Route::post('/facturas/create', 'FacturaController@store')->name("save_factura");

    Route::get('/projectos/{projectId}/adendas', 'AdendaController@index')->name('view_adendas');
    Route::get('/adendas/{adenda_id}/anexos', 'AdendaController@get_anexos')->name('anexos_adendas');
    Route::post('/adendas/create', 'AdendaController@store')->name("save_adenda");

    Route::post('/entregas/','ProjectoController@add_entregas' )->name('add_entregas');

    Route::get('/provincias/{id}/districtos', 'ProvinciaController@districtos');

});
