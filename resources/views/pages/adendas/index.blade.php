@extends('layouts.master')

@section('stylesheets')

@stop
@section('content')

    <div class="container">

        <h1>Lista de Adendas</h1>
        <a href="{{route('details_projectos', [$projecto_id])}}"> < Voltar para projecto</a>
        <hr/>
        <div class="row">

            <div class="col-md-10">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Adicionar Adenda
                </button>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">Data da adenda</th>
                        <th scope="col">Designação</th>
                        <th scope="col">Anexo</th>
                        <th scope="col">Valor</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $adendas as $key=>$adenda)
                        <tr>
                            <td>{{$adenda->data_adenda}}</td>
                            <td>{{$adenda->designacao}}</td>
                            <td><a class="btn btn-secondary" href="{{route('anexos_adendas', $adenda->id)}}"> Ver anexo</a></td>
                            <td>{{number_format($adenda->valor, 0, ',', '.')}} MT</td>
                        </tr>
                    @endforeach
                    <tr><td>Total</td> <td></td> <td></td> <td>{{number_format($valor_sum, 0, ',', '.')}} MT</td></tr>
                    </tbody>
                </table>
                </a>
            </div>

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Adicionar adenda</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{ route('save_adenda') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="projecto_id" value="{{$projecto_id}}">
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <label for="data-pagamento">Data de pagamento</label>
                                        <input type="date" name="data_pagamento" type="text" class="form-control" id="data-pagamento"
                                               placeholder="Data de pagamento">
                                    </div>
                                    <div class="form-group col-md-7">
                                        <label for="valor">Valor</label>
                                        <input type="number" name="valor" type="number" class="form-control" id="valor" placeholder="Valor">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="designacao">Designação</label>
                                        <textarea type="text" name="designacao" type="text" class="form-control" id="designacao"
                                                  placeholder="Designação"></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="input-group col-md-12">
                                        <div class="custom-file">
                                            <input type="file" name="anexo" class="custom-file-input" id="anexo" accept="image/png, image/jpeg, .pdf, .doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                            <label class="custom-file-label" for="anexo">Adicionar anexo</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row form-buttons">
                                    <button type="submit" class="btn btn-primary">Gravar adenda</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('scripts')
@stop
@stop
