@extends('layouts.master')

@section('stylesheets')

@stop
@section('content')

    <div class="container">
        <form method="POST" action="{{ route('create_project') }}">
            @csrf
            <h2>Registar Projecto</h2>
            <hr/>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="designacao">Designação</label>
                    <input name="designacao" type="text" class="form-control" id="designacao" placeholder="Designação">
                </div>
                <div class="form-group col-md-6">
                    <label for="modelo_de_habitacao">Modelo de habitação</label>
                    <select id="modelo_de_habitacao" name="modelo_de_habitacao" class="form-control">
                        <option selected value="vivenda">Vivenda</option>
                        <option value="Vivenda/Isolada"> Vivenda/Isolada</option>
                        <option value="Apartamento"> Apartamento</option>
                        <option value="Geminada"> Geminada</option>
                        <option value="Em Banda"> Em Banda</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="publico-alvo">Público Alvo</label>
                    <select id="publico-alvo" name="publico_alvo" class="form-control">
                        <option selected value="Baixa Renda">Baixa Renda</option>
                        <option value="Média Baixa Renda"> Média Baixa Renda</option>
                        <option value="Média Renda"> Média Renda</option>
                        <option value="Média Alta Renda"> Média Alta Renda</option>
                        <option value="Alta Renda"> Alta Renda</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="provincia">Provincia </label>
                    <select name="provincia" type="text" class="form-control provincias-select" id="provincia" placeholder="Provincia" required>
                        <option value="" >Seleccionar Província</option>
                        @foreach($provincias as $provincia)
                            <option value="{{$provincia->id}}">{{$provincia->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="districto">Districto</label>
                    <select name="districto_id" type="text" class="form-control districtos-select" id="districto" placeholder="Districto" required>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="local">Local </label>
                    <input name="local" type="text" class="form-control" id="local" placeholder="Municipio/ bairro"/>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="coordenadas">Coordenadas</label>
                    <input name="coordenadas" type="text" class="form-control" id="coordenadas"
                           placeholder="Coordenadas">
                </div>
                <div class="form-group col-md-6">
                    <label for="informacao-do-terreno">Informação do terreno</label>
                    <input name="informacao_do_terreno" type="text" class="form-control" id="informacao-do-terreno"
                           placeholder="Informação do terreno">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="empreiteiro">Empreiteiro</label>
                    <input name="empreiteiro" type="text" class="form-control" id="empreiteiro" placeholder="Empreiteiro">
                </div>
                <div class="form-group col-md-6">
                    <label for="fiscal">Fiscal</label>
                    <input name="fiscal" type="text" class="form-control" id="fiscal" placeholder="Fiscal">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="financiador">Fonte de financiamento</label>
                    <select name="financiador" class="form-control" id="financiador" placeholder="Financiador">
                        <option value="OE">Orcamento do Estado (OE)</option>
                        <option value="RP">Receitas Proprias (RP)</option>
                        <option value="Parceria">Parceria</option>
                        <option value="Financiaamento">Financiamento</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="designacao-financiador">Financiador</label>
                    <input name="designacao_financiador" type="text" class="form-control" id="designacao-financiador">
                </div>
            </div>
            <div class="form-row">

                <div class="form-group col-md-12">
                    <label for="descriccao">Descricção</label>
                    <textarea name="descriccao" type="text" class="form-control" id="descriccao"
                              placeholder="Descricção"></textarea>
                </div>
            </div>
            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="data-inicio">Data Início</label>
                    <input name="data_inicio" type="date" class="form-control" id="data-inicio">
                </div>
                <div class="form-group col-md-6">
                    <label for="data-conclusao">Data de Conclusão</label>
                    <input name="data_conclusao" type="date" class="form-control" id="data-conclusao">
                </div>
                <div class="form-group col-md-12">
                    <label for="observacao">Observação</label>
                    <textarea name="observacao" type="text" class="form-control" id="observacao"
                              placeholder="Observação"></textarea>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="avaliacao-projecto">Avaliação do projecto</label>
                    <select name="avaliacao_projecto" class="form-control" id="avaliacao-projecto" placeholder="Avaliação do projecto">
                        <option value="Mau">Mau</option>
                        <option value="Bom">Bom</option>
                        <option value="Muito Bom">Muito Bom</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="estado">Estado</label>
                    <select name="estado" class="form-control" id="estado" placeholder="Estado">
                        <option value="Paralisada">Paralisada</option>
                        <option value="Em curso">Em curso</option>
                        <option value="Concluída">Concluída</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="valor-contrato"> Valor do contrato</label>
                    <input name="valor_contrato" type="number" class="form-control" id="valor-contrato" placeholder="Valor do contrato">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Gravar Projecto</button>
        </form>
    </div>
@section('scripts')
@stop

@stop
