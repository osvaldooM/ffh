@extends('layouts.master')

@section('stylesheets')

@stop
@section('content')

    <div class="container">
        <h1>Detalhes do projecto : {{$projecto->designacao}}</h1>
        <hr/>

        <div class="row">
            <div class="col-md-6">
                <table class="table table-striped">
                    <thead>
                    <tr>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">Designação</th>
                        <td>{{$projecto->designacao}}</td>

                    </tr>
                    <tr>
                        <th scope="row">Modelo de habitação</th>
                        <td>{{$projecto->modelo_de_habitacao}}</td>

                    </tr>
                    <tr>
                        <th scope="row">Público Alvo</th>
                        <td>{{$projecto->publico_alvo}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Local</th>
                        <td>{{$projecto->local}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Coordenadas</th>
                        <td>{{$projecto->coordenadas}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Informação do terreno</th>
                        <td>{{$projecto->informacao_do_terreno}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Empreiteiro</th>
                        <td>{{$projecto->empreiteiro}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Fiscal</th>
                        <td>{{$projecto->fiscal}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Fonte de financiamento</th>
                        <td>{{$projecto->financiador}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Financiador</th>
                        <td>{{$projecto->designacao_financiador}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Descricção</th>
                        <td>{{$projecto->descriccao}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Data Início</th>
                        <td>{{$projecto->data_inicio}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Data de Conclusão</th>
                        <td>{{$projecto->data_conclusao}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Observação</th>
                        <td>{{$projecto->observacao}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Avaliação do projecto</th>
                        <td>{{$projecto->avaliacao_projecto}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Estado</th>
                        <td>{{$projecto->estado}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Valor do contrato</th>
                        <td>{{$projecto->valor_contrato}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-6">
                <a href="{{ route('view_facturas', [$projecto->id]) }}" class="btn btn-primary">Ver facturas</a>

                <a href="{{ route('view_adendas', [$projecto->id]) }}" class="btn btn-primary">Ver adendas</a>
                <hr/>
                <h2>Entregas</h2>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Ano</th>
                        <th>Tipo de produto</th>
                        <th>Modelo</th>
                        <th scope="col">Tipologia de casa</th>
                        <th scope="col">Quantidade</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $projecto->entregas as $index=>$entrega)
                        <tr>
                            <td>{{$entrega->ano}}</td>
                            <td>{{$entrega->tipo_produto}}</td>
                            <td>{{$entrega->modelo_habitacao}}</td>
                            <td>{{$entrega->tipo_casa->tipologia_casa }}</td>
                            <td>{{$entrega->quantidade}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Adicionar entregas
                </button>
            </div>
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Adicionar entregas </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{ route('add_entregas') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="projecto_id" value="{{$projecto->id}}">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="year">Ano</label>
                                    <input type="month" min="1995" max="2050" name="ano" placeholder="ano" class="form-control"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="tipo_produto">Tipo de produto</label>
                                    <select id="tipo_produto" name="tipo_produto" class="form-control">
                                            <option selected  value="casa">casa</option>
                                            <option value="posto policial">posto policial</option>
                                            <option value="escola">escola</option>
                                            <option value="creche">creche</option>
                                            <option value="posto de saúde">posto de saúde</option>
                                            <option value="campo">campo</option>
                                            <option value="supermercado">supermercado</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="modelo">Modelo</label>
                                    <select id="modelo" name="modelo_habitacao" class="form-control">
                                        <option selected value="vivenda">Vivenda</option>
                                        <option value="Vivenda/Isolada"> Vivenda/Isolada</option>
                                        <option value="Apartamento"> Apartamento</option>
                                        <option value="Geminada"> Geminada</option>
                                        <option value="Em Banda"> Em Banda</option>
                                        <option value="Não aplicavel"> Não aplicavel</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="tipo_casa">Tipo de casa</label>
                                    <select id="tipo_casa" name="tipo_casa_id" class="form-control">
                                        @foreach($tipo_casas as $tipo_casa)
                                            <option selected value="{{$tipo_casa->id}}">{{$tipo_casa->tipologia_casa}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-7">
                                    <label for="quantidade">Quantidade</label>
                                    <input type="number" name="quantidade" type="number" class="form-control" id="quantidade"
                                           placeholder="quantidade">
                                </div>
                            </div>
                            <div class="form-row form-buttons">
                                <button type="submit" class="btn btn-primary">Adicionar entrega</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('scripts')
@stop
@stop
