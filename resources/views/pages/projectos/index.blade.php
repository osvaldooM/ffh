@extends('layouts.master')

@section('stylesheets')
@stop
@section('content')

    <div class="container">
        <h1> Lista de Projectos</h1>

        <div class="row">
            @foreach( $projects as $project)
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><strong>{{$project->designacao}}</strong></h5>
                            <p class="card-text">{{$project->local}} - {{$project->estado}}</p>
                            <a href="{{ route('details_projectos', [$project->id]) }}" class="btn btn-primary">Ver Projecto</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@section('scripts')
@stop
@stop
